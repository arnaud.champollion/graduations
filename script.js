// 2024 Arnaud Champollion //
// Flash //
// Licence GNU GPL //


// Consignes

const consigneEleveGenerale = "Écris les nombres dans les bulles puis clique sur le bouton valider.";
const consigneEleveErreur = "Il y a une erreur. Corrige-la puis clique sur le bouton valider.";
const consigneEleveErreurs = "Il y a des erreurs. Corrige-les puis clique sur le bouton valider.";
const consigneEleveToutJuste = "Tous les nombres sont justes, bravo.";
const consigneEleveCorrige = "Voici les bonnes réponses.";

// Constituants de la page
const body = document.body;
const divMilieu = document.getElementById('milieu');
const consigne = document.getElementById('texte-consigne');
const divConsigne = document.getElementById('consigne');
const boutonReplierConsigne = document.getElementById('replier-consigne');

const boutonMenu = document.getElementById('bouton-menu');
const boutonStats = document.getElementById('bouton-stats');
const divContenu = document.getElementById('contenu');
const divReussite = document.getElementById('reussite');
const formulairePas = document.getElementById('choix-pas');
const formulairePassage = document.getElementById('choix-passage');
const droite = document.getElementById('droite');
const divApropos = document.getElementById('apropos');
const checkbox1 = document.getElementById('checkbox1');
const boutonValider = document.getElementById('bouton-valider');
const boutonSuivant = document.getElementById('bouton-suivant');
const spanTauxReussite = document.getElementById('taux-reussite');
const boutonsMasquables = document.querySelectorAll('.masquable');


// Ce préfixe sera utilisé pour le stockage des paramètres dans le cache
let prefixeAppli = 'graduations';

// Variables globales

let dragged = null;
let afficherGraduationsNiveau3 = false;
let nombreItems = 0;
let nombreReussites = 0;
let score = 0;
let scorePourcent = 0;
let passage = 'jamais';
let chances = 2;
let modeAppli = 'general';

let styles = ['minuscules','majuscules','cursive','dyslexic'];

// Réglages de l'interface d'après les variables



// Tableaux

let tableauxPas = [];



// Est-on dans Openboard ?
openboard = Boolean(window.widget || window.sankore);
console.log('Openboard ? '+openboard);

// Adaptations de l'interface pour Openboard
if (openboard){
    document.body.classList.add('openboard');
}

function tailleConsigne() {
    divConsigne.classList.toggle('pliee');
    boutonReplierConsigne.classList.toggle('pliee');
}

function visibiliteMenu(mode){
    if (mode==='ouvre' && menu.style.left==='-415px'){
        menu.style.left='0px';
        menuOn=true;
    }
    else {
        menu.style.left='-415px';
        menuOn=false;
    }
}

function visibiliteStats(mode){
    if (mode==='ouvre' && stats.style.right==='-415px'){stats.style.right='0px';}
    else {stats.style.right='-415px';}
}

function majPassage(valeur,manuel){
    passage = valeur;
    console.log("Mise à jour passage = "+passage)
    stocke('passage',passage);
    majUrl('passage',passage);
    if(manuel){suivant();}
}


function majPas(manuel) {
    // Récupérer tous les éléments input du formulaire
    const checkboxes = formulairePas.querySelectorAll('input[type="checkbox"]');

    // Réinitialiser le tableau
    tableauxPas = [];
    
        
    // Parcourir chaque checkbox
    checkboxes.forEach(checkbox => {
      if (checkbox.checked) {
        const valeur = parseInt(checkbox.value);
        tableauxPas.push(valeur);
      }
    });
    
    // Afficher le tableau pour vérification
    console.log(tableauxPas);
    let pasString = tableauxPas.join('_');
    stocke('pas',pasString);
    majUrl('log',pasString);
    if(manuel){suivant();}
  }

// Vérification des paramètres sont dans l'URL
function checkUrl() {
    console.log('Lecture URL');
    let url = window.location.search;
    let urlParams = new URLSearchParams(url);

    let passageUrl = urlParams.get('passage');
    if (passageUrl){
        passage = passageUrl
    }

    let primtux = urlParams.get('primtuxmenu');
    if (primtux){
        body.classList.add('primtux');
    }

    let modeAppliUrl = urlParams.get('mode');
    if (modeAppliUrl){
        modeAppli = modeAppliUrl;
    }

    let logUrl = urlParams.get('log');
    if (logUrl){
        tableauxPas = logUrl.split('_').map(Number);
    }


}

function appliqueReglages() {
    console.log('Application des réglages');  

    // Cases des pas

    const listeDesCases = document.querySelectorAll('#choix-pas input');
    console.log('tableau pas'+tableauxPas)

    listeDesCases.forEach(caseAcocher => {
        console.log(caseAcocher.value)
        if (tableauxPas.includes(parseInt(caseAcocher.value))){
            caseAcocher.checked = true;
            console.log("coché")
        } else {
            caseAcocher.checked = false;
        }
    });

    body.classList.add(modeAppli);

    mettreAJourChoixPassage();
    
}

function mettreAJourChoixPassage() {
    const radios = document.querySelectorAll('input[name="choix-passage"]');
    console.log("cochage du bouton "+passage)
    radios.forEach(radio => {
        if (radio.value === passage) {
            console.log("bouton radio coché")
            radio.checked = true; // Coche le bouton radio correspondant à la valeur de passage
        } else {
            radio.checked = false; // Décoche les autres
        }
    });
}

async function verifieStockageLocal() {
    console.log('Lecture stockage local');

    const stockagePas = await litDepuisStockage('pas');
    if (stockagePas) {
        tableauxPas = stockagePas.split('_').map(Number);
    }

    const stockgePassage = await litDepuisStockage('passage');
    if (stockgePassage){
        passage = stockgePassage;
    }

}
///////////////////// Lancement du programme ///////////////////
async function executeFunctions() {
    await verifieStockageLocal();
    await checkUrl();
    await appliqueReglages();
    await majPas();
    nouvelExercice();    
}
executeFunctions();
//////////////////////////////////////////////////////////////


function valider() {
    chances -= 1;
    const questions = document.querySelectorAll('.question');
    
    let nombreDeReponsesJustes = 0;
    let nombreDeQuestions = questions.length;

    questions.forEach(question => {

        let reponse = parseFloat(question.innerHTML.replace(/<[^>]*>/g, '').replace(/[^0-9,.]/g, '').replace(',', '.'));
        console.log('contenu bulle = '+question.innerHTML)
        console.log("reponse = "+reponse);
        console.log("réponse attendue = "+question.parentNode.valeur);        

        if (!question.classList.contains('juste')){ // Traitement des questions non répondues ou fausses

            nombreItems += 1;

            if (reponse === question.parentNode.valeur){
                question.classList.add('juste');
                question.contentEditable = false;
                nombreDeReponsesJustes +=1;
                nombreReussites += 1;
            } else {
                question.classList.add('faux');
            }
        } else {nombreDeReponsesJustes +=1;} // Si la question a déjà été répondue

    });
    console.log("Nombre de réussites : "+nombreReussites);
    console.log("Nombre d'items : "+nombreItems);

    // Mise à jour du score
    score = nombreReussites / nombreItems;
    scorePourcent = Math.round(score * 100);
    spanTauxReussite.innerHTML = scorePourcent + '%';
    ///////

    console.log("Nombre de réponses justes : "+nombreDeReponsesJustes);

    if (chances===0 || nombreDeReponsesJustes===nombreDeQuestions){
        boutonSuivant.disabled=false;
        if (modeAppli != 'prof'){
            boutonValider.disabled=true;
        }

        const reponsesFausses = document.querySelectorAll('.faux');

        reponsesFausses.forEach(question => {
            question.classList.remove('faux');
            question.classList.add('corrige');
            question.innerHTML=question.parentNode.valeur.toString().replace('.', ',');
        });

        if (nombreDeReponsesJustes === nombreDeQuestions) {
            consigne.innerHTML=consigneEleveToutJuste;
        } else {
            consigne.innerHTML=consigneEleveCorrige;
        }

    } else {
        if (nombreDeReponsesJustes === nombreDeQuestions - 1) {
            consigne.innerHTML=consigneEleveErreur;
        } else {
            consigne.innerHTML=consigneEleveErreurs;
        }
    }

    

     // Calcul de la couleur
     let r, g, b = 0;  // Pas de composante bleue

     if (scorePourcent < 50) {
         // De 0% à 50% : transition du rouge (255, 0, 0) vers le jaune (255, 255, 0)
         r = 255;
         g = Math.round(255 * (scorePourcent / 50));  // Le vert augmente avec le taux de réussite
     } else {
         // De 50% à 100% : transition du jaune (255, 255, 0) vers le vert (0, 255, 0)
         r = Math.round(255 * (1 - (scorePourcent - 50) / 50));  // Le rouge diminue
         g = 255;
     }
     
     // Appliquer la couleur à divReussite
     divReussite.style.backgroundColor = `rgb(${r}, ${g}, ${b})`;
     
     // Calcul de la luminosité (0.299 * R + 0.587 * G + 0.114 * B)
     let luminosity = 0.299 * r + 0.587 * g + 0.114 * b;
 
     // Choisir noir ou blanc en fonction de la luminosité
     let textColor = (luminosity > 128) ? 'black' : 'white';
 
     // Appliquer la couleur du texte à divReussite
     divReussite.style.color = textColor;

}

function suivant() {
    droite.innerHTML='';
    nouvelExercice();
}



function fin(auto) {
    if (auto || confirm("Arrêter l'exercice en cours ?")){
    boutonsMasquables.forEach(bouton => {
        bouton.classList.add('hide');
    });
    divOptions.classList.add('hide');
    listeMots = [];
    divFin.classList.remove('hide');
    }
}

function nouvelExercice() {

    consigne.innerHTML=consigneEleveGenerale;

    chances = 2;

    if (modeAppli != 'prof'){
        boutonSuivant.disabled=true;
    }
    boutonValider.disabled=false;


    // Tirage au sort du pas

    // Vérification des cases à cocher

    if (tableauxPas.length === 0) {
        checkbox1.checked=true;
        majPas();
    }

    console.log('---------------------')

    const logarithme = tirerDans(tableauxPas);
    console.log("logarithme "+logarithme);
    
    const pas = Math.pow(10, logarithme);
    console.log("pas "+pas);

    const grandPas = Math.pow(10, logarithme+1);
    console.log("grandPas "+grandPas);

    let nbDebut;
    if (passage==='toujours'){
        nbDebut = grandPas * tirerNombreEntier(9,10);
    } else if (passage==='parfois'){
        nbDebut = grandPas * tirerNombreEntier(3,10);
    } else {
        nbDebut = grandPas * tirerNombreEntier(1,9);
    }
    console.log('passage : '+passage)
    console.log("Nombre Début "+nbDebut);

    console.log('---------------------');


    // Création des graduations

    let nombreIntervallesNiveau2 = 20;
    let nombreGraduationsNiveau2 = nombreIntervallesNiveau2 + 1;
    let decalagePremiereGraduationNiveau1 = Math.floor(Math.random() * 6) + 3;
    console.log('Décalage N1 '+decalagePremiereGraduationNiveau1);
    let decalagePremiereGraduationNiveau2 = 10 - decalagePremiereGraduationNiveau1;
    console.log('Décalage N2 '+decalagePremiereGraduationNiveau2);

    let ecart = 100 / (nombreGraduationsNiveau2);

    if (logarithme <  0 && afficherGraduationsNiveau3){
    creeGraduationsNiveau3(0, ecart); // Premières graduations de niveau 3
    }

    for (i = 0; i < nombreIntervallesNiveau2; i++) { // Boucle de création des graduations
        
        let nouvelleGraduation = document.createElement('div');
        nouvelleGraduation.classList.add('graduation');

        let valeur = nbDebut - pas*(decalagePremiereGraduationNiveau1-i);
        console.log('Valeur calculée '+valeur);


        if ((i - decalagePremiereGraduationNiveau1) % 10 === 0){
            let nombreDecimales = Math.max(0, Math.floor(1 - logarithme)); // S'assurer que c'est un entier positif
            nouvelleGraduation.valeur = parseFloat(valeur.toFixed(nombreDecimales));
            console.log('NIVEAU 1 --- Valeur enregistrée'+nouvelleGraduation.valeur);
            nouvelleGraduation.classList.add('niveau1');
            nouvelleEtiquette = document.createElement('div');
            nouvelleEtiquette.classList.add('etiquette');
            nouvelleEtiquette.innerHTML= nouvelleGraduation.valeur.toString().replace('.', ',');
            nouvelleGraduation.appendChild(nouvelleEtiquette);
        } else {
            let nombreDecimales = Math.max(0, Math.floor(-logarithme)); // S'assurer que c'est un entier positif
            nouvelleGraduation.valeur = parseFloat(valeur.toFixed(nombreDecimales));
            console.log('Valeur enregistrée'+nouvelleGraduation.valeur);
            nouvelleGraduation.classList.add('niveau2');
            if ((i - decalagePremiereGraduationNiveau1) % 5 === 0){
                nouvelleGraduation.classList.add('multiple5');
            }
        }
        let position = ecart * (i + 1) ;
        nouvelleGraduation.style.left = position + "%";
        if (logarithme <  0 && afficherGraduationsNiveau3){
            creeGraduationsNiveau3(position, ecart); // Graduations de niveau 3 intermédiaires
        }
        droite.appendChild(nouvelleGraduation);        
    }



    function creeGraduationsNiveau3(position, ecart) {
        for (let k=0; k<9; k++) {
            let nouvelleGraduationNiveau3 = document.createElement('div');
            nouvelleGraduationNiveau3.classList.add('graduation','niveau3');
            nouvelleGraduationNiveau3.style.left =  (position + ecart*(k+1)/ 10) + "%";
            droite.appendChild(nouvelleGraduationNiveau3);
        }
    }

    genererLesQuestions();

}

function genererLesQuestions() {
    let nombreDeQuestions = 5;
    const graduationsNiveau2 = Array.from(document.querySelectorAll('.niveau2'));
    const graduationsChoisies = [];

    // Si le nombre de graduations est inférieur à 5, on ajuste le nombre
    nombreDeQuestions = Math.min(nombreDeQuestions, graduationsNiveau2.length);

    // Tirage aléatoire de graduations
    while (graduationsChoisies.length < nombreDeQuestions) {
        const indexAleatoire = Math.floor(Math.random() * graduationsNiveau2.length);
        const graduationChoisie = graduationsNiveau2[indexAleatoire];
    
        // Vérifiez si la graduation n'est pas déjà choisie
        if (!graduationsChoisies.includes(graduationChoisie)) {
            let estConsecutif = false;
    
            // Vérifiez que la graduation n'est pas adjacente à une des graduations déjà choisies
            for (let i = 0; i < graduationsChoisies.length; i++) {
                const indexGraduationChoisie = graduationsNiveau2.indexOf(graduationChoisie);
                const indexGraduationDejaChoisie = graduationsNiveau2.indexOf(graduationsChoisies[i]);
    
                if (Math.abs(indexGraduationChoisie - indexGraduationDejaChoisie) === 1) {
                    estConsecutif = true;
                    break;
                }
            }
    
            // Si aucune graduation adjacente n'est trouvée, ajoutez la nouvelle graduation
            if (!estConsecutif) {
                graduationsChoisies.push(graduationChoisie);
            }
        }
    }
    


    console.log(graduationsChoisies.length+' questions');


    // Affichage des questions
    graduationsChoisies.forEach(graduation => {
        console.log('question ' + graduation.valeur);
        
        // Crée la nouvelle zone de texte
        nouvelleZoneTexte = document.createElement('div');
        nouvelleZoneTexte.classList.add('etiquette', 'question');
        nouvelleZoneTexte.contentEditable=true;        
        nouvelleZoneTexte.addEventListener('keypress', gererEntreeTexte);
        nouvelleZoneTexte.innerHTML='<br>';
        // Ajoute la nouvelle zone texte à l'élément parent
        graduation.appendChild(nouvelleZoneTexte);

        adapteHauteur(nouvelleZoneTexte);
        
    });

}

function adapteHauteur(question) {
    let graduation = question.parentNode;
    // Vérifie les collisions et ajuste la position si nécessaire
    let toutesLesZonesTexte = document.querySelectorAll('.etiquette.question');
    let collisionTrouvee = true;
    let deplacement = 0; // Variable pour suivre le décalage vertical

    while (collisionTrouvee) {
        collisionTrouvee = false; // Réinitialise à faux avant chaque vérification
        toutesLesZonesTexte.forEach(autreZoneTexte => {
            if (autreZoneTexte !== question) {
                // Vérifie si les éléments se chevauchent
                let rect1 = question.getBoundingClientRect();
                let rect2 = autreZoneTexte.getBoundingClientRect();

                if (!(rect1.bottom < rect2.top || rect1.top > rect2.bottom || rect1.right < rect2.left || rect1.left > rect2.right)) {
                    // Si une collision est détectée, ajuster la position en augmentant 'bottom'
                    let currentBottom = parseFloat(window.getComputedStyle(question).bottom) || 0;
                    question.style.bottom = (currentBottom + 10) + 'px'; // Décale de 10px vers le haut
                    deplacement += 10; // Incrémente le décalage vertical
                    collisionTrouvee = true; // Continue la boucle tant qu'il y a une collision
                }
            }
        });
    }

    // Crée le div de connexion
    let connecteur = document.createElement('div');
    connecteur.classList.add('connecteur'); // Ajoute la classe CSS pour le connecteur
    connecteur.style.height = deplacement + 'px'; // Hauteur correspond au décalage
    //connecteur.style.bottom = (parseFloat(window.getComputedStyle(nouvelleZoneTexte).bottom) - deplacement) + 'px'; // Position en bas de la nouvelle zone texte

    // Ajoute le connecteur à l'élément parent
    graduation.appendChild(connecteur);
}

function gererEntreeTexte(event) {
    // tableau des valeurs autorisées : uniquement les chiffres, points et virgules
    const valeursAutorises = Array.from("1234567890.,");

    // On quitte la méthode si la valeur n'est pas autorisé
    if (!valeursAutorises.includes(event.key)) {
        event.preventDefault();
        return;
    }

    adapteHauteur(event.target);
}


function clic(event){

    console.log('clic');
    let cible = event.target;

    if (cible!=menu && !estEnfantDe(cible,menu) && cible !=boutonMenu){visibiliteMenu('ferme');}
    if (cible!=menu && !estEnfantDe(cible,stats) && cible !=boutonStats){visibiliteStats('ferme');}

    
    if (cible.classList.contains('draggable')) {
        posX = event?.targetTouches?.[0]?.clientX || event.clientX;
        posY = event?.targetTouches?.[0]?.clientY || event.clientY;
        dragged = cible;
        event.preventDefault();
        posX_objet = dragged.offsetLeft;
        posY_objet = dragged.offsetTop;
        diffsourisx = (posX - posX_objet);
        diffsourisy = (posY - posY_objet);
        dragged.classList.add('dragged');
    }

    if (cible.classList.contains('faux')) {
        cible.classList.remove('faux');
    }

}

function move(event) {
    event.preventDefault();  // Empêche le comportement par défaut (utile pour les événements tactiles)

    // Vérifie si un élément est en train d'être "dragged"
    if (dragged) {
        console.log('dragged');
        
        // Obtention des coordonnées en mode tactile ou souris
        const posX = event?.targetTouches?.[0]?.clientX || event.clientX;
        const posY = event?.targetTouches?.[0]?.clientY || event.clientY;

        console.log('posX ' + posX);

        // Calcul des nouvelles positions de l'élément en fonction de la différence entre la souris et l'élément
        dragged.style.left = (posX - diffsourisx) + "px";
        dragged.style.top = (posY - diffsourisy) + "px";
    }
}


function release(event) {
    if (dragged) {
        dragged.classList.remove('dragged');
        dragged=null;
    }

}

// Tactile
document.addEventListener("touchstart", clic);
document.addEventListener("touchend", release);
document.addEventListener('touchmove', function(event) {
    event.preventDefault();
    move(event);
}, { passive: false });
// Souris
document.addEventListener("mousedown", clic);
document.addEventListener("mousemove", move);
document.addEventListener("mouseup", release);


function checkWindowSize() {
    // Ajoute ou enlève la classe "pliee" en fonction de la hauteur de la fenêtre
    if (window.innerHeight <= 465) {
        divConsigne.classList.add('pliee');
        boutonReplierConsigne.classList.add('pliee');
    } else {
        divConsigne.classList.remove('pliee');
        boutonReplierConsigne.classList.remove('pliee');
    }
}

document.addEventListener('DOMContentLoaded', function() {
    // Vérifie la taille de la fenêtre au chargement de la page
    checkWindowSize();
    
    // Vérifie la taille de la fenêtre à chaque redimensionnement
    window.addEventListener('resize', checkWindowSize);
});

